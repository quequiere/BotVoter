package com.quequiere.botvoter;

import java.io.IOException;
import java.net.Proxy;
import java.net.URL;
import java.net.URLStreamHandler;
import java.net.URLStreamHandlerFactory;
import java.util.ArrayList;

import sun.net.www.URLConnection;

public class HandlerFactory implements URLStreamHandlerFactory
{

	public static boolean  first = true;
	@Override
	public URLStreamHandler createURLStreamHandler(String protocol)
	{
		if ("http".equals(protocol))
		{
			return new sun.net.www.protocol.http.Handler()
			{
				@Override
				protected java.net.URLConnection openConnection(URL url, Proxy proxy) throws IOException
				{
					String[] fileParts = url.getFile().split("\\?");
					String contentType = URLConnection.guessContentTypeFromName(fileParts[0]);
					// this small hack is required because, weirdly, svg is not
					// picked up by guessContentTypeFromName
					// because, for Java 8, svg is not in
					// $JAVA_HOME/lib/content-types.properties
					if (fileParts[0].endsWith(".svg"))
					{
						contentType = "image/svg";
					}

					ArrayList<String> banlist = new ArrayList<String>();
					banlist.add("google-analytics");
					banlist.add("googlesyndication");
					banlist.add("twitter");
					banlist.add("facebook");
					banlist.add("nexac");
					banlist.add("adblade");
					banlist.add("scorecard");
					banlist.add("web.css?4.12.5");
					banlist.add("google");

					if ((contentType != null && contentType.contains("image")))
					{
						//System.out.println("banimg: "+url);
						return new URL("rouge.gif").openConnection();
						
					}
					else
					{
						for (String ban : banlist)
						{
							if (url.toString().contains(ban))
							{
								//System.out.println("ban: "+url);
								return new URL("rouge.gif").openConnection();
							}
						}
					
						
				
					
				
						return super.openConnection(url, proxy);
					}
				}
			};
		}

		return null;
	}

}
