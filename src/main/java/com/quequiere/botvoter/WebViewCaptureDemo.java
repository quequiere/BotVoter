package com.quequiere.botvoter;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.*;
import javafx.concurrent.Worker;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.layout.*;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import org.w3c.dom.Node;
import com.quequiere.botvoter.twocaptcha.SolveCaptcha;
import com.sun.webkit.dom.HTMLBodyElementImpl;
import com.sun.webkit.dom.HTMLButtonElementImpl;
import com.sun.webkit.dom.HTMLElementImpl;
import com.sun.webkit.dom.HTMLInputElementImpl;
import com.sun.webkit.dom.HTMLSelectElementImpl;
import com.sun.webkit.dom.HTMLTextAreaElementImpl;
import com.twocaptcha.api.ProxyType;
import com.twocaptcha.api.TwoCaptchaService;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

public class WebViewCaptureDemo extends Application {
	private WebView webView;
	private File captureFile = new File("cap.png");
	public static int start = -1;
	public static String currentName = null;
	
	public static int reloading = 0;

	public static boolean loaded = false;
	public static boolean firstStep = true;
	public static boolean manual = false;
	public static boolean secondWebsite = false;
	
	public static String proxyIP = "";

	public static void main(String[] args) {
		
		if (args.length ==0) {
			System.out.println("You need to specify ip and optional manual ex: 788.59.556.88 manual");
			System.exit(0);
		}
		
		if(args.length>1)
		{
			if(args[1].equals("manual"))
			{
				System.out.println("Manual system enabled !");
				manual=true;
			}
		}
		
		proxyIP=args[0];
		System.out.println("Ip used: "+proxyIP);
		
		
		

		String startFile = "start.txt";
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";

		try {

			br = new BufferedReader(new FileReader(startFile));

			int x = 0;
			while ((line = br.readLine()) != null) {

				start = Integer.parseInt(line);
				System.out.println("start: " + start);
			}
			br.close();

			File myFoo = new File(startFile);
			FileWriter fooWriter = new FileWriter(myFoo, false); // true to
																	// append
																	// false to
																	// overwrite.
			start++;
			fooWriter.write(String.valueOf(start));
			fooWriter.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Application.launch(WebViewCaptureDemo.class);

	}

	@Override
	public void stop() {
		System.out.println("Stop");
		
		try {
			super.stop();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void start(Stage stage) throws Exception {

		loaded = false;

		try {

			URL oracle = new URL("http://"+proxyIP+"/ip.php");
			BufferedReader in = new BufferedReader(new InputStreamReader(oracle.openStream()));

			String inputLine;

			while ((inputLine = in.readLine()) != null) {
				System.out.println("loaded with ip:" + inputLine);

			}

			in.close();

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		String csvFile = "username.csv";
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";

		try {

			br = new BufferedReader(new FileReader(csvFile));

			int x = 0;
			while ((line = br.readLine()) != null) {

				if (x == start) {
					String[] country = line.split(cvsSplitBy);
					currentName = country[0].replace("\"", "");

					break;
				}
				x++;

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		URL.setURLStreamHandlerFactory(new HandlerFactory());

		webView = new WebView();
		webView.setPrefSize(1000, 8000);

		ScrollPane webViewScroll = new ScrollPane();

		webViewScroll.setContent(webView);

		pixelmonservers();
		

		webView.getEngine().load("http://pixelmonservers.com/server/l8YUW2VW/vote");

		HBox controls = null;
		if (manual) {
			controls = new HBox(10);
			final Button capture = new Button("Capture");

			controls.getChildren().addAll(capture);

			capture.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent actionEvent) {

					webView.getEngine().getLoadWorker().cancel();

					System.setProperty("http.proxyHost", proxyIP);
					System.setProperty("http.proxyPort", "8118");
					System.setProperty("https.proxyHost", proxyIP);
					System.setProperty("https.proxyPort", "8118");

					loaded = true;
					webView.setPrefWidth(1000);
					webView.setPrefHeight(2000);

					HTMLInputElementImpl pseudo = (HTMLInputElementImpl) webView.getEngine().getDocument()
							.getElementById("web_server_vote_username");
					pseudo.setValue(currentName);

					String script = "window.scrollTo(0, -400)";
					webView.getEngine().executeScript(script);

					HTMLElementImpl form = (HTMLElementImpl) webView.getEngine().getDocument()
							.getElementById("content");
					HTMLButtonElementImpl button = (HTMLButtonElementImpl) form
							.getElementsByClassName("button alt right").item(0);
					button.click();

				}
			});

		}

		VBox layout = new VBox(10);
		layout.setStyle("-fx-padding: 10; -fx-background-color: cornsilk;");
		if (controls != null) {
			layout.getChildren().setAll(webViewScroll, controls);
		} else {
			layout.getChildren().setAll(webViewScroll);
		}

		stage.setScene(new Scene(layout));
		stage.show();
		
	}

	public void pixelmonservers() {
		
		
		webView.getEngine().getLoadWorker().stateProperty().addListener(new ChangeListener<Worker.State>() {
			@Override
			public void changed(ObservableValue<? extends Worker.State> observable, Worker.State oldValue,
					Worker.State newValue) {

				if (newValue == Worker.State.SUCCEEDED && !loaded && firstStep) {
					

					new java.util.Timer().schedule(new java.util.TimerTask() {
						@Override
						public void run() {

							Platform.runLater(new Runnable() {
								@Override
								public void run() {
									
								

									// System.out.println(webView.getEngine().executeScript("document.documentElement.innerHTML"));
									 //System.out.println(webView.getEngine().executeScript("document.documentElement.outerHTML"));
									firstPart();
								}
							});

						}
					}, 2000);

				} else if (manual && newValue == Worker.State.SUCCEEDED && firstStep) {
					System.out.println("exit");

					new java.util.Timer().schedule(new java.util.TimerTask() {
						@Override
						public void run() {
							System.out.println("Execute exit !");
							Platform.exit();
							System.exit(0);
						}
					}, 3000);

					return;
				} else if (newValue == Worker.State.SUCCEEDED && loaded && firstStep) {
					
					
					if(!secondWebsite)
					{
						System.out.println("exit");

						new java.util.Timer().schedule(new java.util.TimerTask() {
							@Override
							public void run() {
								System.out.println("Execute exit !");
								Platform.exit();
								System.exit(0);
							}
						}, 3000);

						return;
					}
					
					

					firstStep = false;
					loaded = false;
					webView.getEngine().load("https://pixelmon-server-list.com/server/53/vote");
				} else if (newValue == Worker.State.SUCCEEDED && !loaded && !firstStep
						|| newValue == Worker.State.FAILED && !loaded && !firstStep) {
					loaded = true;

					HTMLBodyElementImpl body = null;

					for (int x = 0; x < webView.getEngine().getDocument().getChildNodes().item(1).getChildNodes()
							.getLength(); x++) {
						Node n = webView.getEngine().getDocument().getChildNodes().item(1).getChildNodes().item(x);
						if (n.getClass().equals(HTMLBodyElementImpl.class)) {
							body = (HTMLBodyElementImpl) n;
							
						}

					}

					HTMLInputElementImpl pseudo = (HTMLInputElementImpl) body.getElementsByClassName("form-control")
							.item(1);

					pseudo.setValue(currentName);

					String html = (String) webView.getEngine().executeScript("document.documentElement.outerHTML");
					html = html.split("data-sitekey=\"")[1];
					html = html.split("\"></div>")[0];
					html = html.split("\"><div><div")[0];
					System.out.println("Recaptchakey is:" + html);

					String apiKey = "20a2c402a37b5b5a6821d241b6724f5c";
					String googleKey = html;
					;
					String pageUrl = "https://pixelmon-server-list.com/server/53/vote";
					String proxyIp = proxyIP;
					String proxyPort = "8118";
					String proxyUser = "";
					String proxyPw = "";

					TwoCaptchaService service = new TwoCaptchaService(apiKey, googleKey, pageUrl, proxyIp, proxyPort,
							proxyUser, proxyPw, ProxyType.HTTP);

					try {
						String responseToken = service.solveCaptcha();
						System.out.println("The response token is: " + responseToken);
						HTMLTextAreaElementImpl googlecap = (HTMLTextAreaElementImpl) webView.getEngine().getDocument()
								.getElementById("g-recaptcha-response");
						googlecap.setValue(responseToken);
					} catch (InterruptedException e) {
						System.out.println("ERROR case 1");
						e.printStackTrace();
					} catch (IOException e) {
						System.out.println("ERROR case 2");
						e.printStackTrace();
					}

					HTMLButtonElementImpl button = (HTMLButtonElementImpl) body
							.getElementsByClassName("btn btn-success btn-lg center-block").item(0);
					button.click();

				} else if (newValue == Worker.State.SUCCEEDED && loaded && !firstStep) {
					System.out.println("Exit scheduled !");

					new java.util.Timer().schedule(new java.util.TimerTask() {
						@Override
						public void run() {
							System.out.println("Execute exit !");
							Platform.exit();
							System.exit(0);
						}
					}, 5000);

				}

			}
		});
	}

	public void firstPart() {

	
		
		System.setProperty("http.proxyHost", proxyIP);
		System.setProperty("http.proxyPort", "8118");
		System.setProperty("https.proxyHost", proxyIP);
		System.setProperty("https.proxyPort", "8118");

		loaded = true;
		webView.setPrefWidth(1000);
		webView.setPrefHeight(2000);




		SnapshotParameters s = new SnapshotParameters();
		

		s.setViewport(new Rectangle2D(10, 650, 310, 160));

	

		if (manual) {
		
			return;
		}

		WritableImage image = webView.snapshot(s, null);
		BufferedImage bufferedImage = SwingFXUtils.fromFXImage(image, null);
		
		if( !passcolor(bufferedImage, 0, 0, 305, 160))
		{
			
			if(reloading>=3)
			{
			
				System.out.println("Exit cause can't load captcha !");
				
				
				new java.util.Timer().schedule(new java.util.TimerTask() {
					@Override
					public void run() {
						System.out.println("Execute exit !");
						Platform.exit();
						System.exit(0);
					}
				}, 3000);
				
				
				return;
				
			}
			
			System.out.println("reloading captcha");
			webView.getEngine().executeScript("javascript:ACPuzzle.reload('')");
			
			reloading++;
			
			new java.util.Timer().schedule(new java.util.TimerTask() {
				@Override
				public void run() {

					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							firstPart();
						}
					});

				}
			}, 8000);
			
			
			
			
			
		}
		else
		{
			System.out.println("captcha valide");
			saveAndUploadImage(bufferedImage);
		}


	}
	
	
	
	public void saveAndUploadImage(BufferedImage bufferedImage)
	{
		try {
			
			ImageIO.write(bufferedImage, "png", captureFile);

			System.out.println("Captured WebView to: " + captureFile.getAbsoluteFile());
			
			
	
			
			

					 //System.out.println(webView.getEngine().executeScript("document.documentElement.innerHTML"));
					// System.out.println(webView.getEngine().executeScript("document.documentElement.outerHTML"));
					 
					 //if(1==1)
						// return;
			
			
			if(!(webView.getEngine().getDocument().getElementById("adcopy_response") instanceof HTMLInputElementImpl))
			{
				System.out.println("text list exit !");
				new java.util.Timer().schedule(new java.util.TimerTask() {
					@Override
					public void run() {
						System.out.println("Execute exit !");
						Platform.exit();
						System.exit(0);
					}
				}, 3000);
				
				return;
			}
			
			
			HTMLInputElementImpl pseudo = (HTMLInputElementImpl) webView.getEngine().getDocument()
					.getElementById("web_server_vote_username");
			
			HTMLInputElementImpl keyfield = (HTMLInputElementImpl) webView.getEngine().getDocument()
					.getElementById("adcopy_response");
			
			

			String key = "empty";
			try {
				key = SolveCaptcha.start(captureFile.getAbsolutePath());
			} catch (URISyntaxException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			System.out.println("Solved: " + key);

		

			
			pseudo.setValue(currentName);

		
			keyfield.setValue(key);

			HTMLElementImpl form = (HTMLElementImpl) webView.getEngine().getDocument().getElementById("content");
			HTMLButtonElementImpl button = (HTMLButtonElementImpl) form.getElementsByClassName("button alt right")
					.item(0);
			button.click();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static boolean passcolor(BufferedImage bi, int x0, int y0, int w,
	        int h) {
	    int x1 = x0 + w;
	    int y1 = y0 + h;
	    long sumr = 0, sumg = 0, sumb = 0;
	    for (int x = x0; x < x1; x++) {
	        for (int y = y0; y < y1; y++) {
	            Color pixel = new Color(bi.getRGB(x, y));
	            sumr += pixel.getRed();
	            sumg += pixel.getGreen();
	            sumb += pixel.getBlue();
	        }
	    }
	    int num = w * h;
	    
	    long a = sumr/num;
	    long b = sumg/num;
	    long c = sumb/num;
	    
	    long moy = (a+b+c)/3;
	    
	    System.out.println("moy: "+moy);

	    if(moy>242 && moy<244||moy==238||moy==247)
	    {
	    	return false;
	    }
	    
	    return true;
	}

}
