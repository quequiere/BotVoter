package com.quequiere.botvoter.twocaptcha;
import static org.apache.http.client.fluent.Form.form;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URIBuilder;

public class SolveCaptcha {

	static String API_KEY = "20a2c402a37b5b5a6821d241b6724f5c";

	public static String start(String path) throws IOException, URISyntaxException, InterruptedException {
		String submissionId = sendCaptcha(path);
		if (!submissionId.isEmpty()) {
			// CAPCHA_NOT_READY
			String captcha = getCaptchaString(submissionId);
			while (captcha.equals("CAPCHA_NOT_READY")) {
				captcha = getCaptchaString(submissionId);
				Thread.sleep(5000);
			}

			// final captcha
			return captcha.substring(3);
		}
		
		return "errorlocal3";
	}

	private static String getCaptchaString(String submissionId)
			throws ClientProtocolException, IOException, URISyntaxException {
		URIBuilder builder = new URIBuilder();
		builder.setScheme("http").setHost("2captcha.com").setPath("/res.php").setParameter("key", API_KEY)
				.setParameter("action", "get").setParameter("id", submissionId);

		return Request.Get(builder.build()).execute().returnContent().asString();
	}

	private static String sendCaptcha(String fileName) throws IOException {
		byte[] img = Files.readAllBytes(Paths.get(fileName));
		String base64Image = Base64.getEncoder().encodeToString(img);

		String response = Request.Post("http://2captcha.com/in.php")
				.bodyForm(form().add("method", "base64").add("key", API_KEY).add("body", base64Image).build()).execute()
				.returnContent().asString();
		if (response.startsWith("OK|")) {
			// our CAPTCHA was accepted, return the ID
			return response.substring(3);
		} else {
			// something went wrong
			return "here";
		}
	}

}
